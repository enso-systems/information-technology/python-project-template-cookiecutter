#!/usr/bin/env bash

set -euo pipefail

echo "=============== Container successfully started. ==============="
echo
echo "===============          Python Version         ==============="
python -c "import sys; print(sys.version_info)"
echo
echo "===============    Installed Python Packages    ==============="
pip list
echo
echo "===============       Environment Variables     ==============="
env
echo
echo "===============      Executing Main Command     ==============="
echo "python -c 'import sys; print(sys.version_info)'"
exec python -c "import sys; print(sys.version_info)"
